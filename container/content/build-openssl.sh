#!/bin/bash

if ! [ -d "/home/builder/build/src/openssl" ]; then
    cd $HOME/build/src
    git clone https://github.com/openssl/openssl.git
fi

cd $HOME/build/src/openssl
git fetch
git checkout OpenSSL_1_1_1-stable
git pull

./config --prefix=$HOME/build/inst --openssldir=$HOME/build/inst
make -j 8
make install_dev
rm -f $HOME/build/inst/lib/libcrypto.so*
rm -f $HOME/build/inst/lib64/libcrypto.so*
rm -f $HOME/build/inst/lib/libssl.so*
rm -f $HOME/build/inst/lib64/libssl.so*
