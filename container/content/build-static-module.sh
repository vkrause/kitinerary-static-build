#!/bin/bash

module=$1
if [ -z "$module" ]; then
    echo "Usage $0 <module>"
    exit 1
fi

build-openssl.sh
kdesrc-build --rc-file ~/config/$module-buildrc
kdesrc-build --rc-file ~/config/$module-buildrc --include-dependencies $module

for i in `cat ~/config/$module.output`; do
    destdir=`dirname $i`
    mkdir -p ~/output/$destdir
    cp -rf ~/build/inst/$i ~/output/$destdir
    rm -f ~/output/$i.debug
done

find ~/output/bin -type f | xargs -n 1 strip -s -x
